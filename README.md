# VisualStudio Snippets

## Class 
| *Shortcut*  | *Description*                        |
|--|--|
| clss 	      | Create class with regions            |
| clsscenario | Create class for scenario            |
| clsutest    | Create class for unit testing        |
| clsitest    | Create class for integration testing |


## Properties

> **Public property definition:**
> public type Name  
> {
> &nbsp;&nbsp;&nbsp;&nbsp;get;
> &nbsp;&nbsp;&nbsp;&nbsp;set;
> }

> **Private property definition:**
> 
> public type _name  
> {
> &nbsp;&nbsp;&nbsp;&nbsp;get;
> &nbsp;&nbsp;&nbsp;&nbsp;set;
> }

| *Shortcut* | *Description*            |
|--|--|
|ppstr       |Private string property   |
|pstr        |Public string property    |
|ppint       |Private int property      |
|pint        |Public int property       |
|ppdou       |Private double property   |
|pdou        |Public double property    |
|ppdec       |Private decimal property  |
|pdec        |Public decimal property   |
|ppbol       |Private boolean property  |
|pbol        |Public boolean property   |
|ppflo       |Private float property    |
|pflo        |Public float property     |


## Regions

> #region RegionName
> 
> #endregion

| *Shortcut* | *Description*            |
|--|--|
|rect        |Constructors              |
|repm        |Private methods           |
|rem         |Public methods            |
|repp        |Private properties        |
|rep         |Public properties         |